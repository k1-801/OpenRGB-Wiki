# AMD Radeon 6000

 Similar to the Wraith Spire before it the AMD branded Radeon
GPUs have an RGB controller provided by Coolermaster.

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `2516` | `014D` | Cooler Master Radeon 6000 GPU |
| `2516` | `015B` | Cooler Master Radeon 6900 GPU |
