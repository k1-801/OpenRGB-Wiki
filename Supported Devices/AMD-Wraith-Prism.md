# AMD Wraith Prism

 The Wraith Prism comes with 2 cables but is only detectable
and controlable when using the USB cable. `Morse Code` and `Mirage`
modes have not been implemented. Saving to flash is supported by
the device but not yet implemented.

## Connection Type
 USB

## Saving
 Not currently supported by OpenRGB

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not fully implemented by controller (See device page for details)

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `2516` | `0051` | AMD Wraith Prism |
