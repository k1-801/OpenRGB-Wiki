# ASRock ASR RGB SMBus

 ASRock ASR RGB LED controllers will save with each update.
Per ARGB LED support is not possible with these devices.

## Connection Type
 SMBus

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `:` | `:` |  |
