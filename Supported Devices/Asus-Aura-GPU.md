# Asus Aura GPU

 

## Connection Type
 SMBus

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `10DE:1C81` | `1043:85D8` | ASUS ROG Strix GTX 1050 O2G Gaming |
| `10DE:1C82` | `1043:8611` | ASUS ROG Strix GTX 1050 TI 4G Gaming |
| `10DE:1C82` | `1043:85CD` | ASUS ROG Strix GTX 1050 TI O4G Gaming |
| `10DE:1C82` | `1043:85D1` | ASUS ROG Strix GTX 1050 TI O4G Gaming |
| `10DE:1C03` | `1043:85A4` | ASUS GTX 1060 Strix 6G Gaming |
| `10DE:1C03` | `1043:85AC` | ASUS GTX 1060 Strix |
| `10DE:1B81` | `1043:8598` | ASUS GTX 1070 Strix Gaming |
| `10DE:1B81` | `1043:8599` | ASUS GTX 1070 Strix OC |
| `10DE:1B82` | `1043:861D` | ASUS ROG Strix GTX1070 Ti 8G Gaming |
| `10DE:1B82` | `1043:861E` | ASUS ROG Strix GTX1070 Ti A8G Gaming |
| `10DE:1B80` | `1043:8592` | ASUS GTX 1080 Strix OC |
| `10DE:1B80` | `1043:85AA` | ASUS ROG Strix GTX1080 A8G Gaming |
| `10DE:1B80` | `1043:85F9` | ASUS ROG Strix GTX1080 O8G Gaming |
| `10DE:1B80` | `1043:85E8` | ASUS ROG Strix GTX1080 O8G 11Gbps |
| `10DE:1B06` | `1043:85EB` | ASUS ROG Strix GTX1080 Ti Gaming |
| `10DE:1B06` | `1043:85F1` | ASUS ROG Strix GTX1080 Ti 11G Gaming |
| `10DE:1B06` | `1043:85EA` | ASUS ROG Strix GTX1080 Ti O11G Gaming |
| `10DE:1B06` | `1043:85E4` | ASUS ROG Strix GTX1080 Ti O11G Gaming |
| `10DE:2187` | `1043:874F` | ASUS ROG Strix GTX 1650S OC 4G |
| `10DE:21C4` | `1043:8752` | ASUS ROG Strix GTX 1660S O6G Gaming |
| `10DE:2182` | `1043:86A5` | ASUS ROG GTX 1660 Ti OC 6G |
| `10DE:1F08` | `1043:86D3` | ASUS ROG STRIX RTX 2060 EVO Gaming 6G |
| `10DE:1F08` | `1043:868E` | ASUS ROG STRIX RTX 2060 O6G Gaming |
| `10DE:1F08` | `1043:86D2` | ASUS ROG STRIX RTX 2060 O6G Gaming |
| `10DE:1E89` | `1043:8775` | ASUS ROG STRIX RTX 2060 O6G EVO Gaming |
| `10DE:1F06` | `1043:8730` | ASUS ROG STRIX RTX 2060S 8G Gaming |
| `10DE:1F47` | `1043:8703` | ASUS ROG STRIX RTX 2060S A8G EVO Gaming |
| `10DE:1F06` | `1043:86FB` | ASUS ROG STRIX RTX 2060S O8G Gaming |
| `10DE:1F06` | `1043:86FC` | ASUS ROG STRIX RTX 2060S A8G Gaming |
| `10DE:1F07` | `1043:8671` | ASUS ROG STRIX RTX 2070 A8G Gaming |
| `10DE:1F07` | `1043:8670` | ASUS ROG STRIX RTX 2070 O8G Gaming |
| `10DE:1EC7` | `1043:86FF` | ASUS ROG STRIX RTX 2070S A8G Gaming |
| `10DE:1E84` | `1043:8728` | ASUS ROG STRIX RTX 2070S A8G Gaming |
| `10DE:1E84` | `1043:8706` | ASUS ROG STRIX RTX 2070S A8G Gaming |
| `10DE:1E84` | `1043:8729` | ASUS ROG STRIX RTX 2070S O8G Gaming |
| `10DE:1E84` | `1043:8707` | ASUS ROG STRIX RTX 2070S 8G Gaming |
| `10DE:1E84` | `1043:8727` | ASUS ROG STRIX RTX 2070S O8G Gaming |
| `10DE:1E82` | `1043:867F` | ASUS ROG STRIX RTX 2080 8G Gaming |
| `10DE:1E87` | `1043:865F` | ASUS ROG STRIX RTX 2080 O8G Gaming |
| `10DE:1E87` | `1043:8661` | ASUS ROG STRIX RTX 2080 O8G V2 Gaming |
| `10DE:1E81` | `1043:8712` | ASUS ROG STRIX RTX 2080S A8G Gaming |
| `10DE:1E81` | `1043:8711` | ASUS ROG STRIX RTX 2080S O8G Gaming |
| `10DE:1E81` | `1043:876B` | ASUS ROG STRIX RTX 2080S O8G White |
| `10DE:1E04` | `1043:8687` | ASUS ROG STRIX RTX 2080 Ti 11G Gaming |
| `10DE:1E07` | `1043:866C` | ASUS ROG STRIX RTX 2080 Ti 11G Gaming |
| `10DE:1E07` | `1043:866B` | ASUS ROG STRIX RTX 2080 Ti A11G Gaming |
| `10DE:1E07` | `1043:866A` | ASUS ROG STRIX RTX 2080 Ti O11G Gaming |
| `10DE:2489` | `1043:87C6` | ASUS TUF RTX 3060 Ti 8G Gaming OC |
| `1002:687F` | `1043:0555` | ASUS AREZ Strix RX Vega 56 O8G |
| `1002:687F` | `1043:04C4` | ASUS Vega 64 Strix |
| `1002:731F` | `1043:04EC` | ASUS RX 5600XT Strix O6G Gaming |
| `1002:731F` | `1043:04E2` | ASUS RX 5700XT Strix Gaming OC |
| `1002:73BF` | `1043:04F6` | ASUS RX 6800 TUF Gaming OC |
| `1002:67DF` | `1043:04B0` | ASUS ROG STRIX RX470 O4G GAMING |
| `1002:67DF` | `1043:04FB` | ASUS ROG STRIX RX480 Gaming OC |
| `1002:67FF` | `1043:04BC` | ASUS ROG STRIX RX560 Gaming |
| `1002:67DF` | `1043:04C2` | ASUS RX 570 Strix O4G Gaming OC |
| `1002:67DF` | `1043:0517` | ASUS RX 580 Strix Gaming OC |
| `1002:67DF` | `1043:0519` | ASUS RX 580 Strix Gaming TOP |
