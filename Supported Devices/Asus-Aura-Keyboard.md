# Asus Aura Keyboard

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `193C` | ASUS ROG Falchion (Wired) |
| `0B05` | `193E` | ASUS ROG Falchion (Wireless) |
| `0B05` | `1875` | ASUS ROG Strix Flare |
| `0B05` | `18CF` | ASUS ROG Strix Flare PNK LTD |
| `0B05` | `18F8` | ASUS ROG Strix Scope |
| `0B05` | `1951` | ASUS ROG Strix Scope RX |
| `0B05` | `190C` | ASUS ROG Strix Scope TKL |
| `0B05` | `1954` | ASUS ROG Strix Scope TKL PNK LTD |
