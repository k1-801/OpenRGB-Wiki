# Asus Aura Monitor

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `198C` | ASUS ROG Strix XG27AQ |
| `0B05` | `19BB` | ASUS ROG Strix XG27AQM |
| `0B05` | `1933` | ASUS ROG Strix XG27W |
| `0B05` | `19B9` | ASUS ROG PG32UQ |
