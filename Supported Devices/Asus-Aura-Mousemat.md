# Asus Aura Mousemat

 

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `1891` | ASUS ROG Balteus |
| `0B05` | `1890` | ASUS ROG Balteus Qi |
