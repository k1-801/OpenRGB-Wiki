# Asus Aura TUF Keyboard

 

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `184D` | ASUS ROG Claymore |
| `0B05` | `18AA` | ASUS TUF Gaming K7 |
| `0B05` | `194B` | ASUS TUF Gaming K3 |
