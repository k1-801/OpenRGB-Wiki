# BloodyMouse

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `09DA` | `37EA` | Bloody W60 Pro |
| `09DA` | `FA60` | Bloody MP 50RS |
