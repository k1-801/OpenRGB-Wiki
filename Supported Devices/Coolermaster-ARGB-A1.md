# Coolermaster ARGB A1

 This device does not have Gen 2 support in OpenRGB yet.

Gen2 has auto-resize feature and parallel to serial magical stuff.<

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `2516` | `0173` | Cooler Master ARGB Gen 2 A1 |
| `2516` | `01C9` | Cooler Master ARGB Gen 2 A1 V2 |
