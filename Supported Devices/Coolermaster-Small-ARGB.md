# Coolermaster Small ARGB

 The Coolermaster Small ARGB device supports `Direct` mode
from firmware 0012 onwards. Check the serial number for the date
"A202104052336" or newer.

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `2516` | `1000` | Cooler Master Small ARGB |
