# Intel Arc A770 Limited Edition

 

## Connection Type
 USB

## Saving
 Not currently supported by OpenRGB

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not fully implemented by controller (See device page for details)

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `2516` | `01B5` | Intel Arc A770 Limited Edition |
