# Logitech Lightspeed

 The Lightspeed controller is the generic RGB Controller
for all Logitech HID++ devices that support feature page 8070.

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `046D` | `405D` | Logitech G403 Wireless Gaming Mouse |
| `046D` | `407F` | Logitech G502 Wireless Gaming Mouse |
| `046D` | `4070` | Logitech G703 Wireless Gaming Mouse |
| `046D` | `4086` | Logitech G703 Hero Wireless Gaming Mouse |
| `046D` | `4053` | Logitech G900 Wireless Gaming Mouse |
| `046D` | `4067` | Logitech G903 Wireless Gaming Mouse |
| `046D` | `4087` | Logitech G903 Hero Wireless Gaming Mouse |
| `046D` | `4079` | Logitech G Pro Wireless Gaming Mouse |
| `046D` | `405F` | Logitech Powerplay Mat |
| `046D` | `C332` | Logitech G502 Proteus Spectrum Gaming Mouse |
| `046D` | `C08B` | Logitech G502 Hero Gaming Mouse |
| `046D` | `C083` | Logitech G403 Prodigy Gaming Mouse |
| `046D` | `C082` | Logitech G403 Wireless Gaming Mouse (wired) |
| `046D` | `C08D` | Logitech G502 Wireless Gaming Mouse (wired) |
| `046D` | `C087` | Logitech G703 Wireless Gaming Mouse (wired) |
| `046D` | `C090` | Logitech G703 Hero Wireless Gaming Mouse (wired) |
| `046D` | `C081` | Logitech G900 Wireless Gaming Mouse (wired) |
| `046D` | `C086` | Logitech G903 Wireless Gaming Mouse (wired) |
| `046D` | `C091` | Logitech G903 Hero Wireless Gaming Mouse (wired) |
| `046D` | `C088` | Logitech G Pro Wireless Gaming Mouse (wired) |
| `046D` | `0AB5` | Logitech G733 Gaming Headset |
| `046D` | `0A87` | Logitech G935 Gaming Headset |
