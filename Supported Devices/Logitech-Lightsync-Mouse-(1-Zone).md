# Logitech Lightsync Mouse (1 Zone)

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `046D` | `C084` | Logitech G203 Prodigy |
| `046D` | `C092` | Logitech G203 Lightsync |
| `046D` | `C085` | Logitech G Pro Gaming Mouse |
| `046D` | `C08C` | Logitech G Pro (HERO) Gaming Mouse |
