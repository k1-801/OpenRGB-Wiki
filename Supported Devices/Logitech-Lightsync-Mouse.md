# Logitech Lightsync Mouse

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `046D` | `C080` | Logitech G303 Daedalus Apex |
| `046D` | `C08F` | Logitech G403 Hero |
