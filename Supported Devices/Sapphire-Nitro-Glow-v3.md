# Sapphire Nitro Glow v3

 

## Connection Type
 I2C

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `1002:731F` | `1DA2:E409` | Sapphire RX 5700 (XT) Nitro+ |
| `1002:731F` | `1DA2:E410` | Sapphire RX 5700 XT Nitro+ |
| `1002:731F` | `1DA2:426E` | Sapphire RX 5700 XT Nitro+ |
| `1002:7340` | `1DA2:E423` | Sapphire RX 5500 XT Nitro+ |
| `1002:73BF` | `1DA2:438E` | Sapphire RX 6800 XT Nitro+ SE |
| `1002:73BF` | `1DA2:E438` | Sapphire RX 6800 XT/6900 XT Nitro+ |
| `1002:73BF` | `1DA2:E439` | Sapphire RX 6800 Nitro+ |
| `1002:73DF` | `1DA2:E445` | Sapphire RX 6700 XT Nitro+ |
| `1002:73FF` | `1DA2:E448` | Sapphire RX 6600 XT Nitro+ |
| `1002:73BF` | `1DA2:440E` | Sapphire RX 6900 XT Nitro+ SE |
| `1002:73A5` | `1DA2:441D` | Sapphire RX 6950 XT Nitro+ |
| `1002:73AF` | `1DA2:F440` | Sapphire RX 6900 XT Toxic |
| `1002:73BF` | `1DA2:F441` | Sapphire RX 6900 XT Toxic Limited Edition |
| `1002:73BF` | `1DA2:440F` | Sapphire RX 6900 XT Toxic |
