# Zet Blade Optical

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `2EA8` | `2125` | ZET Blade Optical |
